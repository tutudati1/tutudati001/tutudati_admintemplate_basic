import { reactive, toRaw } from 'vue'

// 分页钩子函数
interface Options {
    page?: number
    size?: number
    fetchFun: (_arg: any) => Promise<any>
    params?: Record<any, any>
    firstLoading?: boolean
}

export function usePaging(options: Options) {
    const { page = 1, size = 15, fetchFun, params = {}, firstLoading = false } = options
    // 记录分页初始参数
    const paramsInit: Record<any, any> = Object.assign({}, toRaw(params))
    // 分页数据
    const pager = reactive({
        page,
        size,
        loading: firstLoading,
        count: 0,
        lists: [] as any[],
        extend: {} as Record<string, any>
    })
    // 请求分页接口
    const getLists = () => {
        pager.loading = true
        return fetchFun({
            page_no: pager.page,
            page_size: pager.size,
            ...params
        })
            .then((res: any) => {
                pager.count = res?.count
                pager.lists = res?.lists
                pager.extend = res?.extend
                return Promise.resolve(res)
            })
            .catch((err: any) => {
                return Promise.reject(err)
            })
            .finally(() => {
                pager.loading = false
            })
    }
    // 重置为第一页
    const resetPage = () => {
        pager.page = 1
        getLists()
    }
    // 重置参数
    const resetParams = () => {
        Object.keys(paramsInit).forEach((item) => {
            params[item] = paramsInit[item]
        })
        getLists()
    }
    return {
        pager,
        getLists,
        resetParams,
        resetPage
    }
}

export function useReadingPaging(options: Options) {
    const { page = 1, size = 15, fetchFun, params = {}, firstLoading = false } = options
    // 记录分页初始参数
    const paramsInit: Record<any, any> = Object.assign({}, toRaw(params))
    // 分页数据
    const pagerReading = reactive({
        page,
        size,
        loading: firstLoading,
        count: 0,
        lists: [] as any[],
        extend: {} as Record<string, any>
    })
    // 请求分页接口
    const getListsReading = () => {
        pagerReading.loading = true
        return fetchFun({
            page_no: pagerReading.page,
            page_size: pagerReading.size,
            ...params
        })
            .then((res: any) => {
                pagerReading.count = res?.count
                pagerReading.lists = res?.lists
                pagerReading.extend = res?.extend
                return Promise.resolve(res)
            })
            .catch((err: any) => {
                return Promise.reject(err)
            })
            .finally(() => {
                pagerReading.loading = false
            })
    }
    // 重置为第一页
    const resetPageReading = () => {
        pagerReading.page = 1
        getListsReading()
    }
    // 重置参数
    const resetParamsReading = () => {
        Object.keys(paramsInit).forEach((item) => {
            params[item] = paramsInit[item]
        })
        getListsReading()
    }
    return {
        pagerReading,
        getListsReading,
        resetParamsReading,
        resetPageReading
    }
}

export function useJudePaging(options: Options) {
    const { page = 1, size = 15, fetchFun, params = {}, firstLoading = false } = options
    // 记录分页初始参数
    const paramsInit: Record<any, any> = Object.assign({}, toRaw(params))
    // 分页数据
    const pagerJude = reactive({
        page,
        size,
        loading: firstLoading,
        count: 0,
        lists: [] as any[],
        extend: {} as Record<string, any>
    })
    // 请求分页接口
    const getListsRJude = () => {
        pagerJude.loading = true
        return fetchFun({
            page_no: pagerJude.page,
            page_size: pagerJude.size,
            ...params
        })
            .then((res: any) => {
                pagerJude.count = res?.count
                pagerJude.lists = res?.lists
                pagerJude.extend = res?.extend
                return Promise.resolve(res)
            })
            .catch((err: any) => {
                return Promise.reject(err)
            })
            .finally(() => {
                pagerJude.loading = false
            })
    }
    // 重置为第一页
    const resetPageJude = () => {
        pagerJude.page = 1
        getListsRJude()
    }
    // 重置参数
    const resetParamsJude = () => {
        Object.keys(paramsInit).forEach((item) => {
            params[item] = paramsInit[item]
        })
        getListsRJude()
    }
    return {
        pagerJude,
        getListsRJude,
        resetParamsJude,
        resetPageJude
    }
}
