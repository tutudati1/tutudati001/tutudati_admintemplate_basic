import request from '@/utils/request'

// 用户端菜单列表
export function apiMenuLists(params: any) {
    return request.get({ url: '/menu.menu/lists', params })
}

// 添加用户端菜单
export function apiMenuAdd(params: any) {
    return request.post({ url: '/menu.menu/add', params })
}

// 编辑用户端菜单
export function apiMenuEdit(params: any) {
    return request.post({ url: '/menu.menu/edit', params })
}

// 删除用户端菜单
export function apiMenuDelete(params: any) {
    return request.post({ url: '/menu.menu/delete', params })
}

// 用户端菜单详情
export function apiMenuDetail(params: any) {
    return request.get({ url: '/menu.menu/detail', params })
}