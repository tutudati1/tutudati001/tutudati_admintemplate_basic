import request from '@/utils/request'

// 轮播图列表
export function apiBannerLists(params: any) {
    return request.get({ url: '/banner.banner/lists', params })
}

// 添加轮播图
export function apiBannerAdd(params: any) {
    return request.post({ url: '/banner.banner/add', params })
}

// 编辑轮播图
export function apiBannerEdit(params: any) {
    return request.post({ url: '/banner.banner/edit', params })
}

// 删除轮播图
export function apiBannerDelete(params: any) {
    return request.post({ url: '/banner.banner/delete', params })
}

// 轮播图详情
export function apiBannerDetail(params: any) {
    return request.get({ url: '/banner.banner/detail', params })
}