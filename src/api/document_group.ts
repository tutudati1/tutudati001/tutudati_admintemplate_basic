import request from '@/utils/request'

// 手册栏目列表
export function apiDocumentGroupLists(params: any) {
    return request.get({ url: '/help.document_group/lists', params })
}

// 添加手册栏目
export function apiDocumentGroupAdd(params: any) {
    return request.post({ url: '/help.document_group/add', params })
}

// 编辑手册栏目
export function apiDocumentGroupEdit(params: any) {
    return request.post({ url: '/help.document_group/edit', params })
}

// 删除手册栏目
export function apiDocumentGroupDelete(params: any) {
    return request.post({ url: '/help.document_group/delete', params })
}

// 手册栏目详情
export function apiDocumentGroupDetail(params: any) {
    return request.get({ url: '/help.document_group/detail', params })
}

export function apiDocumentGroupAll(params: any) {
    return request.get({ url: '/help.document_group/getAll', params })
}