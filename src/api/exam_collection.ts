import request from '@/utils/request'

// 试卷列表
export function apiExamCollectionLists(params: any) {
    return request.get({ url: '/exam.exam_collection/lists', params })
}

// 添加试卷
export function apiExamCollectionAdd(params: any) {
    return request.post({ url: '/exam.exam_collection/add', params })
}

// 编辑试卷
export function apiExamCollectionEdit(params: any) {
    return request.post({ url: '/exam.exam_collection/edit', params })
}

// 删除试卷
export function apiExamCollectionDelete(params: any) {
    return request.post({ url: '/exam.exam_collection/delete', params })
}

// 试卷详情
export function apiExamCollectionDetail(params: any) {
    return request.get({ url: '/exam.exam_collection/detail', params })
}

// 绑定试题
export function apiExamCollectionBindExam(params: any) {
    return request.post({ url: '/exam.exam_collection/bindCollection', params })
}

// 试卷答题记录
export function apiExamCollectionHistory(params: any) {
    return request.get({ url: '/exam.exam_collection_submit/lists', params })
}

// 导出历史记录
export function apiExamCollectionExport(params: any) {
    return request.get({ url: '/exam.exam_collection/export', params })
}

// 试卷表单记录
export function apiExamFormHistory(params: any) {
    return request.get({ url: '/exam.exam_form/lists', params })
}

// 导出表单记录
export function apiExamFormExport(params: any) {
    return request.get({ url: '/exam.exam_form/export', params })
}
